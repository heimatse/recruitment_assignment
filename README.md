# Problem description

* Write a program as described under the "Program description" section below.
* Create a git repo (if you haven't already) and commit the code you've written.
* Push the git repo to github (or similar public git service such as bitbucket or gitlab)
* Ensure that it is public-readable!
* Then send us a link to the repo.
* Don't spend more than 45 minutes or so on this. It's OK if you don't finish! Commit what you have, or show
  us the code without pushing it.

## Program description

Write a small command-line application. Use whatever language you feel most comfortable with.

* The application should take a single argument: a string representing a person's occupation, e.g.
    `Salesperson`

* The application should read a csv file that contains the following data:

```
name,occupation,age
Eileen,salesperson,31
Charlie,Salesperson,23
Bob,"Sofware Developer",53
Alice,CEO,49
Dave,"Bus Driver",37
```

You can hard-code the name of the file (e.g. always load a file called `data.csv`)

* The application should go through the csv data, and in alphabetical order print all the unique people whose
occupation matches that of the input argument.

* If no occupation was given (that is, the program was called without any arguments), a message should
    be printed to the standard output that describes how to use the program.

* When finding the occupation entered by the user, perform a case-insenstiive match. Both `salesperson` and
  `sAlesPersOn` should match an entry for `Salesperson`.


## Example

* Example command line usage, assuming a solution written in Python3 as a single file 
  `"find_people_with_occupation.py"`:

```
$ cat <<EOF > data.csv
name,occupation,age
Eileen,salesperson,31
Bob,"Sofware Developer",53
Alice,CEO,49
Dave,"Bus Driver",37
Charlie,Salesperson,23
EOF

$ python3 find_people_with_occupation.py
usage: solution.py [-h] occupation

Prints the unique names of all people with a certain occupation

positional arguments:
  occupation  The occupation of a person. This field is case-insensitive.

optional arguments:
  -h, --help  show this help message and exit

$ python3 find_people_with_occupation.py salesperson
Charlie
Eileen

$ python3 find_people_with_occupation.py "Bus Driver"
Dave

$ python3 find_people_with_occupation.py doesnotexist

(no output)
```

### Note

* You can use any possible tools! Use google, stack overflow, read documentation, phone a friend, ask us for
  help. Ask us if you need any clarifications. Ask questions!

* Act as if this was a first iteration of a program you'd write at work, to be used by other developers.
  Being an early iteration, it's OK if not all cases are considered or that not all data is validated.

* We don't want you to hand-roll a csv parser! You wouldn't do that at work, so don't do it here either.

* While this problem is trivially small, it's OK to assume that it's used as part of a bigger system,
   and it is perfectly fine to depend on third party libraries if you want.
   For example, your language's standard library may not come with a nice-to-use argument parsing or csv
   parsing library. While adding them might be overkill for this small problem, it's okay to add these
   dependencies as they'll probably be used elsewhere too.
